package main;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Cliente{
    public static void main(String[] arg){
        Cliente cliente = new Cliente();
        cliente.connectServer();
    }

    private void connectServer() {
        try {
            int resultado;

            Registry registro = LocateRegistry.getRegistry("192.168.0.108", 7777);
            Operaciones interfaz = (Operaciones) registro.lookup("RemotoRMI");
            resultado = interfaz.suma(9, 2);
            System.out.println("El resultado es: " + resultado);
        } catch (RemoteException | NotBoundException ex) {
            System.out.println("error: " + ex.getMessage());
        }
    }
}
